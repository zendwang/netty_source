# netty源码分析注解

#### 介绍
netty源码注解4.1.51（注解）

#### 软件架构
软件架构说明


#### 安装教程

1.  源码本地调试之编译过程
参考：http://www.zyiz.net/tech/detail-134911.html和https://blog.csdn.net/wuyinxian/article/details/46382051  
netty源码本地调试之编译过程  
①：github下载源码，本次版本为4.1.51  
②：本地调试入口，example包下实例类：io.netty.example.http.websocketx.server.WebSocketServer  
③：debug启动此类会报错，提示如下，这是因为有些类是由groovy动态生成的（io.netty.util.collection不存在）这是因为一些包是动态生成的    
④：这里进入到common下面（在terminal窗口中cd common命令编译进入netty-common目录）运行mvn compile。发现报错,关于maven-checkstyle-plugin，这是编译的一些格式检查，编译的时候可以跳过
mvn compile -Dcheckstyle.skip=true -DskipTest   //跳过测试类和格式检查  
⑤：打包：还是在common下运行打包命令：mvn clean package -Dcheckstyle.skip=true -Dmaven.test.skip=true，通过就可以启动测试样例入口WebSocketServer了  

2.  xxxx

3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
